$(document).ready(function() {
  if (Drupal.settings.mediaslideshow) {
    Drupal.mediaslideshowAttach();
  }  
});

Drupal.mediaslideshowAttach = function() {
  for (var node in Drupal.settings.mediaslideshow) {
    var mediaslideshow = Drupal.settings.mediaslideshow[node], container = $('.mediaslideshow-' + node);
    
    function preloadImages(i) {
      var n = mediaslideshow.images[++i];
      if (mediaslideshow.current != i) {
        if (n) {
          n.image = $('<img />').src(n.src).load(function() { preloadImages(i); });
        } else preloadImages(0);
      }
    }
    
    function updatemediaslideshow(previous) {
      if (mediaslideshow.current != previous) {
	    $('#thumb-' + previous).removeClass('active');
	    $('#thumb-' + mediaslideshow.current).addClass('active');
	  }
	  
	  var current = mediaslideshow.images[mediaslideshow.current];
      current.title = current.title || '';
      current.description = current.description || '';
      current.href = current.href || '#';

      container.        
        find('.mediapolaroid').src(current.src).css({ opacity: 0.8 }).animate({ opacity: 1, width: current.width, height: current.height }).end().
        find('.title').html(current.title).end().find('.description').html(current.description).end().
        find('.link').href(current.href).end().find('.current').html(mediaslideshow.current).end();
      return false;
    }

    container.find('.previous').click(function() {
	  var current = mediaslideshow.current;
	  if(!(mediaslideshow.images[--mediaslideshow.current])) mediaslideshow.current = mediaslideshow.total;
      return updatemediaslideshow(current);
    }).end().
    
    find('.next').click(function() {
	  var current = mediaslideshow.current;
      if(!(mediaslideshow.images[++mediaslideshow.current])) mediaslideshow.current = 1;
      return updatemediaslideshow(current);
    }).end();
    
    $('.mediaslideshow-thumb').each(function () {
	  $(this).click(function() {
		var previous = mediaslideshow.current;
		mediaslideshow.current = this.id.substr(6, this.id.length-6);
		return updatemediaslideshow(previous);
      });
    });

    preloadImages(mediaslideshow.current);	
	$('#thumb-' + mediaslideshow.current).addClass('active');
  }
}
