<?php
/**
 * @file
 * Views API hook implementations for the node_media module.
 */

function _node_media_views_tables() {
  $tables['node_media'] = array(
    'name' => 'node_media',
    'provider' => 'internal',
    'join' => array(
      'left' => array(
        'table' => 'node',
        'field' => 'nid'
      ),
      'right' => array(
        'field' => 'nid'
      ),
    ),
    'fields' => array(
      'node_media_display' => array(
        'name' => t('Node media: Display node media'),
        'notafield' => TRUE,
        'query_handler' => '_node_media_query_handler_display',
        'handler' => array(
          '_node_media_views_handler_image_thumb' => t('Thumbnails'),
          '_node_media_views_handler_image_fullsize' => t('Full size'),
        ),
        'sortable' => false,
        'help' => t('Display all node media in one field.'),
      ),
    ),
    'filters' => array(
      'nid' => array(
        'name' => t('Node media: Has node media'),
        'operator' => array('=' => t('Exists')),
        'list' => 'views_handler_operator_yesno',
        'list-type' => 'select',
        'handler' => '_node_media_handler_images_exist',
        'help' => t('Filter whether the node has node media.'),
      ),
    ),
  );
  return $tables;
}

function _node_media_query_handler_display($field, $fieldinfo, &$query) {
  $query->add_field('nid', 'node');
}

/**
 * Views handler for displaying images.
 */
function _node_media_views_handler_image_thumb($fieldinfo, $fielddata, $value, $data, $thumb = TRUE) {
  $node = node_load(array('nid' => $data->nid));
  $format = ($thumb ? 'thumbs' :'fullsize');
  $return = theme('node_media_view', $node, TRUE, FALSE, $format);
  return $return[0];
}

/**
 * Views handler for displaying images in links to a popup window
 */
function _node_media_views_handler_image_fullsize($fieldinfo, $fielddata, $value, $data) {
  return _node_media_views_handler_image_thumb($fieldinfo, $fielddata, $value, $data, FALSE);
}

/**
 * Views handler for filtering
 */
function _node_media_handler_images_exist($op, $filter, $filterdata, &$query) {
  switch ($op) {
    case 'handler':
      $query->ensure_table('node_media');
      if ($filter['value']) {
        $query->set_distinct();
        $table_data = _views_get_tables();
        $joins = array('type' => 'inner');
        $joins = array_merge($joins, $table_data['node_media']['join']);
        $query->joins['node_media'][1] = $joins;
      }
      else {
        $query->add_where('ISNULL(node_media.id)');
      }
  }
}
